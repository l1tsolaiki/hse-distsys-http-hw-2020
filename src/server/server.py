import uuid
from datetime import datetime
from typing import Optional

import uvicorn
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel

app = FastAPI(title=__name__)


goals = {}
datefmt = '%d/%m/%Y'


class Goal(BaseModel):
    name: str
    deadline: str
    responsible: str
    goalID: Optional[str]


def getID():
    return str(uuid.uuid4())


@app.get('/get')
async def getGoals():
    return sorted(goals.values(), key=lambda goal: datetime.strptime(goal.deadline, datefmt))


@app.get('/get/{goalID}')
async def getGoal(goalID: str):
    value = goals.get(goalID)
    if value is None:
        raise HTTPException(status_code=400, detail=f'Goal with goalID={goalID} does not exist')
    return value


@app.post('/add')
async def addGoal(goal: Goal):
    try:
        datetime.strptime(goal.deadline, datefmt)
    except ValueError:
        raise HTTPException(status_code=400, detail=f"Date '{goal.deadline}' does not match format '{datefmt}'")
    goalID = getID()
    goal.goalID = goalID
    goals[goalID] = goal
    return goal


@app.put('/update/{goalID}')
async def modifyGoal(goalID: str, goal: Goal):
    goal.goalID = goalID
    goals[goalID] = goal
    return goal


@app.delete('/delete/{goalID}')
async def deleteGoal(goalID: str):
    value = goals.pop(goalID, None)
    if value is None:
        raise HTTPException(status_code=400, detail=f'Goal with goalID={goalID} does not exist')
    return value


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=80)
