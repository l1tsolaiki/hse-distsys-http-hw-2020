# Документация на мой API

Это приложение для создания, чтения, модификации и удаления сущностей *Goal*.
Сущность представляет из себя некотурую цель. У неё есть название/описание. 
У неё есть дедлайн - время до которого планируется цель достигнуть.
Также есть ответственный, который занимается выполнением цели.
При добавлении в приложение цель получает уникальный goalID, и дальнейшая работа производится по goalID.

Формат даты: '%d/%m/%Y'.


* *POST* (Create);
  
  URL = "/add"
  
  Клиент передаёт json вида:
  ```json
  "name": str
  "deadline": str
  "responsible": str
  "goalID": Optional[str]
  ```
  Response: id сохраненного объекта со статсом 200 (в случае отсутствия хотя бы одного из полей, выводит пустое тело со статусом 400)
  
* *GET* (Read);
  
  URL = "/get"
  
  Response: все существующие goal

  или

  URL = "/get/goalID", где goalID - ID цели
  
  Response: goal с ID == goalID
* *PUT* (Update)
    
  URL = "/update/goalID", где goalID - ID цели
  
  Body: json с объектом goal
  
  Response: обновлённый goal вместе с ID.

* *DELETE* (Delete)
    
  URL = "/delete/goalID", где goalID - ID цели
  
  Response: удалённый goal
