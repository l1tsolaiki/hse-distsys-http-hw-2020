import json
import os

import requests

SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_FLASK_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)

IDS = []


def test_post():
    payload = {
        'name': 'Learn 2 code',
        'responsible': 'l1tsolaiki',
        'deadline': '12/12/2020'
    }

    response = requests.post(URL + '/add', json=payload)
    assert response.status_code == 200
    data = json.loads(response.text)
    IDS.append(data['goalID'])


def test_get():
    payload = {
        'name': 'Learn ML',
        'responsible': 'ltybc',
        'deadline': '01/02/2021'
    }

    response = requests.post(URL + '/add', json=payload)
    assert response.status_code == 200
    data = json.loads(response.text)
    goalID = data['goalID']
    IDS.append(goalID)

    response = requests.get(URL + '/get/')
    assert response.status_code == 200

    response = requests.get(URL + f'/get/{goalID}')
    assert response.status_code == 200

    response = requests.get(URL + f'/get/abc')
    assert response.status_code == 400


def test_update():
    goalID = IDS[0]
    response = requests.get(URL + f'/get/{goalID}')
    assert response.status_code == 200
    data = json.loads(response.text)

    data['responsible'] = 'ltybc'

    response = requests.put(URL + f'/update/{goalID}', json=data)
    assert response.status_code == 200
    data = json.loads(response.text)

    response = requests.get(URL + f'/get/{goalID}')
    assert response.status_code == 200
    data = json.loads(response.text)
    assert data['responsible'] == 'ltybc'


def test_delete():
    for goalID in IDS:
        response = requests.delete(URL + f'/delete/{goalID}')
        assert response.status_code == 200
        data = json.loads(response.text)
    response = requests.get(URL + '/get/')
    assert response.status_code == 200
    data = json.loads(response.text)
    assert len(data) == 0
